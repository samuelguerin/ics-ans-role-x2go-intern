# ics-ans-role-x2go

Ansible role to install x2go.

## Role Variables

```yaml
...
```

## Example Playbook

```yaml
- hosts: servers
  roles:
    - role: ics-ans-role-x2go
```

## License

BSD 2-clause
